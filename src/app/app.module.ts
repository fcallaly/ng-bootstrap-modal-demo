import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ComponentModalComponent } from './component-modal/component-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentModalComponent
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ComponentModalComponent]
})
export class AppModule { }
